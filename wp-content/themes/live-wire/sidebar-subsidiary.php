<?php
/**
 * Subsidiary Sidebar Template
 *
 * Displays widgets for the Subsidiary dynamic sidebar if any have been added to the sidebar through the 
 * widgets screen in the admin by the user.  Otherwise, nothing is displayed.
 *
 * @package Live Wire
 * @subpackage Template
 * @since 0.1.0
 */

if ( is_active_sidebar( 'subsidiary' ) ) : ?>

	<?php do_atomic( 'before_sidebar_subsidiary' ); // live-wire_before_sidebar_subsidiary ?>

		<div id="sidebar-subsidiary" class="sidebar">
		
			<div class="wrap">
			<?php if ( is_front_page() ):?>
			<div class="main_footer_con">
				<div class="footer_con"><?php get_template_part( 'loop-meta' ); // Loads the loop-meta.php template. ?>

			<?php if ( have_posts() ) : ?>

				<?php while ( have_posts() ) : the_post(); ?>

					<?php get_template_part( 'content', ( post_type_supports( get_post_type(), 'post-formats' ) ? get_post_format() : get_post_type() ) ); ?>
<div style="width:100%; height:auto; ">
					
</div>
				<?php endwhile; ?>

			<?php else : ?>

			<?php get_template_part( 'loop-error' ); // Loads the loop-error.php template. ?>

			<?php endif; ?><br/></div>
			<div style="float:left; padding-left:20px;">
			<?php if ( is_front_page() ):?>
			
			 <?php dynamic_sidebar('sidebar-2'); the_widget( 'sidebar-2', $instance, $args); ?>
			 <?php endif; ?>
			</div>
			</div>
			<div style="width:100%; float:left;">
				<?php endif; ?>

				<?php do_atomic( 'open_sidebar_subsidiary' ); // live-wire_open_sidebar_subsidiary ?>

				<?php dynamic_sidebar( 'subsidiary' ); ?>

				<?php do_atomic( 'close_sidebar_subsidiary' ); // live-wire_close_sidebar_subsidiary ?>
				
			</div><!-- .wrap -->

		</div><!-- #sidebar-subsidiary .aside -->
		
	<?php do_atomic( 'after_sidebar_subsidiary' ); // live-wire_after_sidebar_subsidiary ?>
	</div>

<?php endif; ?>