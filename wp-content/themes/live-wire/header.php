<?php
/**
 * Header Template
 *
 * The header template is generally used on every page of your site. Nearly all other templates call it 
 * somewhere near the top of the file. It is used mostly as an opening wrapper, which is closed with the 
 * footer.php file. It also executes key functions needed by the theme, child themes, and plugins. 
 *
 * @package Live Wire
 * @subpackage Template
 * @since 0.1.0
 */
?>
<!doctype html>
<html <?php language_attributes(); ?>>

<head>
<meta http-equiv="Content-Type" content="<?php bloginfo( 'html_type' ); ?>; charset=<?php bloginfo( 'charset' ); ?>" />
<title><?php hybrid_document_title(); ?></title>

<!-- Mobile viewport optimized -->
<meta name="viewport" content="width=device-width,initial-scale=1" />

<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

<?php wp_head(); // wp_head ?>

</head>

<body class="<?php hybrid_body_class(); ?>">

	<?php do_atomic( 'open_body' ); // live-wire_open_body ?>

	<div id="container" itemscope itemtype="http://schema.org/ElementarySchools">
	
	
		<?php get_template_part( 'menu', 'primary' ); // Loads the menu-primary.php template. ?>
		
		<?php do_atomic( 'before_header' ); // live-wire_before_header ?>

			<div id="header">
					
			<?php do_atomic( 'open_header' ); // live-wire_open_header ?>
	
				<div class="wrap">

					<div id="branding">
					<meta content="Marlboro Montessori Academy" itemprop="name"/>
					<meta content="Marlboro Montessori near Matawan is one of the most renowned private schools in NJ. We offer summer & day camps at our montessori located near Matawan, NJ." itemprop="description"/>
					<a href="/mmademo4">	<img src="http://marlboromontessoriacademy.com/mmademo4/logo.png" alt="Marlboro Montessri Academy, Preschool, Elementary School and Summer Camp" itemprop="logo"/></a>
					
					</div><!-- #branding -->
					
					<?php get_sidebar( 'header' ); // Loads the sidebar-header.php template. ?>
							
					<?php do_atomic( 'header' ); // live-wire_header ?>

				</div><!-- .wrap -->
						
				<?php do_atomic( 'close_header' ); // live-wire_close_header ?>

			</div><!-- #header -->
	
		<?php do_atomic( 'after_header' ); // live-wire_after_header ?>
		
		<?php get_template_part( 'menu', 'secondary' ); // Loads the menu-secondary.php template. ?>
		
		<?php do_atomic( 'before_main' ); // live-wire_before_main ?>
		
		<?php if ( is_front_page() ):?>
		<div class="metaslider">
			<?php 
    echo do_shortcode("[metaslider id=435]"); 
?><br /></div>
<?php endif; ?>
		<div id="main">

			<div class="wrap">
			
			<?php do_atomic( 'open_main' ); // live-wire_open_main ?>
			
			